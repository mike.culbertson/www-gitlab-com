---
layout: markdown_page
title: Product Stage Direction - Manage
description: "The Manage stage in GitLab delights business stakeholders and enables organizations to work more efficiently"
canonical_path: "/direction/manage/"
---

## On this page
{:.no_toc}

- TOC
{:toc}


<p align="center">
    <font size="+2">
        <b>Manage your GitLab workspace and view your DevOps adoption to increase efficiency and be compliant</b>
    </font>
</p>

<%= partial("direction/manage/templates/overview") %>

<%= devops_diagram(["Manage"]) %>

## Stage Overview
The Manage stage in GitLab **delights business stakeholders and enables organizations to work more efficiently**. Managing a piece of software is more than maintaining infrastructure; tools like GitLab need to be adoptable by companies of all sizes and be easy to operate. Setting up your processes shouldn’t be a struggle, and administrators shouldn’t have to compromise on security or compliance to prevent tools from hindering their velocity.

* **Delighting the business**: to make GitLab adoptable by organizations of any size, it must excel at meeting table stakes that are set by the business. A skyscraper with many people in it can only be enabled by a solid, secure foundation - an application serving a similar scale isn't any different. GitLab needs to support the access control, onboarding, security, and auditing needs that enables enterprise-level scale. We also need to make the foundation easy to lay; constructing a building is slow and arduous when it's done brick-by-brick. Adopting GitLab should be fast and reliable and show a quick trail to getting an amazing return on your investment in GitLab.
* **Working more efficiently**: while we want to fulfill foundational needs, GitLab strives to give you the ability to work in new and powerful ways. We aspire to answer valuable questions for users and to automate away the mundane. It’s not enough to give instances the ability to meet their most basic needs; as a single application for the DevOps lifecycle, GitLab can exceed the standard and enable you to work in ways you previously couldn’t.

## Our Groups
Manage is composed of 4 distinct groups that support our mission of **delighting business stakeholders and enabling organizations to work more efficiently**. We plan with 3 timeframes in mind:

* Our **vision** looks ahead ~3 years toward an ambitious future state. Our stage has a vision, and so do each of our groups.
* Our **goals** help provide measurable objectives that are conditions of success for us to realize our vision. We can't realize our stage/group vision without accomplishing these things.
* Finally, **What's Next** are areas of immediate, tactical focus covering the next ~3 releases related to our goals.

All of these sections are intended to be short (no more than a few bullets) and help guide our decisions. They live in parallel with our [category direction pages](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/doc/templates/product/category_direction_template.html.md), which go into more detail on specific capabilities and are not constrained to specific timeframes.
* If an area of interest isn't explicitly mentioned in our Goals or What's Next, please assume that we're not focused on it and won't devote a significant amount of attention to it.

### Access
Access provides leadership on some of GitLab's most foundational capabilities - access control, authentication/authorization, permissions, and subgroups - that enable users to get into GitLab quickly and start getting work done. If we're doing our job correctly, an instance should be able to get a new user onboarded into GitLab without friction: at the right time, with the right level of permissions, to the right resources - all with a great user experience for the new user and the administrator doing the configuration and maintenance.

### Optimize
Optimize helps organizations more quickly recognize the value of their innovations in two ways:
* Providing visibility into value streams and highlighting waste to drive GitLab users toward improvement opportunities
* Increasing the effectiveness and efficiency of knowledge workers in daily work, prompting decisionmaking that maximizes benefit across the whole value stream

Counter-productive local optimizations are a natural result of a limited field-of-view. By creating situtional awareness informed by analytics and value-stream thinking, we give every GitLab user the superpower of extraordinary insight and efficiency.

### Compliance

<%= partial("direction/manage/compliance/templates/overview") %>

### Import
Import's goal is to make the transition into GitLab seamless. Instances rarely begin completely from scratch; almost every organization has existing repositories, projects, and resources that sit on the outside of any new tool after its been adopted. The more we do to help new instances and users start flying in GitLab, the faster they're able to realize value out of the application - and the longer they'll stay.

## Vision

<%= partial("direction/manage/templates/themes") %>
## Goals for 2021

<%= partial("direction/manage/templates/goals") %>

## What's Next

### Access

1. Improve the GitLab.com administration experience with [enterprise users](https://gitlab.com/groups/gitlab-org/-/epics/4786), SAML enforcement on [API](https://gitlab.com/gitlab-org/gitlab/-/issues/297389),

2. Build the [workspace](https://docs.gitlab.com/ee/user/workspace/) concept. This will be the top-level [namespace](https://docs.gitlab.com/ee/user/group/index.html#namespaces) for you to manage everything GitLab. We will start by introducing issues into this top level group via [gitlab&4257](https://gitlab.com/groups/gitlab-org/-/epics/4257). This includes simplifying settings management for both GitLab deployment models with [workspaces and cascading settings](https://gitlab.com/groups/gitlab-org/-/epics/4419).

3. Policies MVC to deny [project deletion](https://gitlab.com/gitlab-org/gitlab/-/issues/34693) and [membership management](https://gitlab.com/gitlab-org/gitlab/-/issues/234099).

4. [SAML group sync for self-managed](https://gitlab.com/gitlab-org/gitlab/-/issues/118)

#### What we're not doing
* Improvements to user profiles and user management, as defined on the [Users direction page](https://about.gitlab.com/direction/manage/users/#users)

### Optimize
In pursuit of these goals, Optimize is currently focused on two initiatives:

1. [Enterprise DevOps reporting](https://gitlab.com/groups/gitlab-org/-/epics/4066): understanding instance-level adoption of GitLab is a blind spot for customers of large instances. We'd like to solve for two main problems for executives and leaders: tracking GitLab's ROI and helping find centers of excellence. We'll prioritize work like an [instance-level MVC](https://gitlab.com/gitlab-org/gitlab/-/issues/193435).

2. Value stream analytics: we will continue building upon the efficiency metrics available through the Value Stream feature, tying them in to DevOps reporting to highlight DevOps best practices that can lead to faster and more reliable software development, while also allowing engineering management to drill in and take action. The value of this work should be immediately apparent to GitLab's Engineering and Quality Departments. We will seek opportunities to [dogfood](https://gitlab.com/groups/gitlab-org/-/epics/3894) new features, collect feedback from internal teams, and iterate.

### Compliance
* Increase the number of `workflow::ready for development` issues by spending 50% of our time on issue refinement each milestone.
* Continuing our work to [refactor audit events](https://gitlab.com/groups/gitlab-org/-/epics/2765) to improve overall performance and reduce implementation complexity
* Ship critical group-level features - [merge request approvals](https://gitlab.com/groups/gitlab-org/-/epics/4367) and [required compliance pipelines](https://gitlab.com/groups/gitlab-org/-/epics/3156) - to meet the needs of regulated enterprise customers
* Iterate on the [Compliance Dashboard](https://gitlab.com/groups/gitlab-org/-/epics/2537), [Credential Inventory](https://gitlab.com/groups/gitlab-org/-/epics/4110), and [Audit Events](https://gitlab.com/groups/gitlab-org/-/epics/1217) to strengthen these features for compliance-minded organizations

To ensure we build a cohesive Compliance experience at GitLab, the following points of view are a baseline for other GitLab product groups to consider.

### Import
1. To provide a path for our customers moving from GitHost to GitLab.com, the Import group is currently focused on enabling GitLab.com adoption by [starting to iterate](https://gitlab.com/groups/gitlab-org/-/epics/4374) toward the [complete solution](https://gitlab.com/groups/gitlab-org/-/epics/2771) for GitLab-to-GitLab migrations.
2. Additionally, we'll be focused on improving our GitHub to GitLab importer by iterating our way toward the [complete GitHub importer](https://gitlab.com/groups/gitlab-org/-/epics/2860#proposed-solution). We will start by delivering a [scaled experience](https://gitlab.com/groups/gitlab-org/-/epics/5042) that makes migrations from GitHub quick and easy even for the largest organizations.

#### What we're not doing
* No new importers are planned at this time.
* Group Import will prioritize only critical bugs and security fixes for the Internationalization functionality.

## Metrics
Manage uses [Stage MAU](https://about.gitlab.com/handbook/product/metrics/#stage-monthly-active-users-smau) as a primary measure of success. This represents the unique number of users getting value from the stage; all groups should be able to contribute to improving this number.

Manage's Stage MAU is currently being improved. Please see [this issue](https://gitlab.com/gitlab-org/manage/general-discussion/-/issues/17231) to track progress.

Individual groups track progress against a number of group-specific [performance indicators](https://about.gitlab.com/handbook/product/metrics/):

| Group | Dashboard URL |
| ------ | ------ |
| Access | [MAU Using Paid SAML](https://about.gitlab.com/handbook/product/dev-section-performance-indicators/#manageaccess---paid-gmau---mau-using-paid-saml)|
| Optimize | [MAU Viewing Analytics Features](https://about.gitlab.com/handbook/product/dev-section-performance-indicators/#manage-manageoptimize---smau-gmau---mau-viewing-analytics-features) |
| Compliance | [MAU Viewing Compliance Features](https://about.gitlab.com/handbook/product/dev-section-performance-indicators/#managecompliance---paid-gmau---mau-viewing-compliance-features)|
| Import | [MAU importing](https://about.gitlab.com/handbook/product/dev-section-performance-indicators/#manageimport---gmau---mau-importing)|

## How we operate
Manage operates under GitLab's values, but is a stage that seeks to particularly excel in certain areas that support our goals above. We seek to be leaders at GitLab by:

### Iterate on the essential
* Leading the way on iteration, regularly shooting for small but ambitious MVCs.
* Supporting iteration with a great planning and development process, giving us checkpoints to keep issues small and incremental. As a result, our throughput is high.
* Valuing the 1-year themes above, and deliberately deciding to not pursue initiatives that don’t support our 2020 goals. We'd rather do a few things well than a bunch of things poorly.
* Prioritizing depth over breadth. For the most part, we’re biased toward doubling down and investing on what’s working rather than extending the breadth of our stage.

### Measure what matters
* Prioritizing instrumentation through our North Star dashboards, which we regularly monitor to keep our priorities in check.
* Measuring business value by tying customer delight and revenue to our priorities.

### Great team
* Aspiring to be the happiest team at GitLab, with high individual job satisfaction.
* Having great work-life balance, ensuring that we [value friends and family above work](https://about.gitlab.com/handbook/values/#family-and-friends-first-work-second) and avoid individual burnout.

<%= partial("direction/categories", :locals => { :stageKey => "manage" }) %>

## Pricing
<%= partial("direction/manage/templates/pricing") %>

## Upcoming Releases

<%= direction["all"]["all"] %>

<%= partial("direction/other", :locals => { :stage => "manage" }) %>

<p align="center">
    <i><br>
    Last Reviewed: 2021-08-04<br>
    Last Updated: 2021-08-04
    </i>
</p>
